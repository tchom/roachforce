class Command:
	const NAME = "virtual_command"
	func _init(args = []):
		pass

	func execute(args = []):
		pass

class InitialiseMapCommand extends Command:
	const MapSettings = preload("res://Scripts/Settings/map_settings.gd")
	const NAME = "MapCommand.initialise_map_command"

	func execute(args = []):
		var pin_locations = args[0]
		MapModel.initilialise(pin_locations)

class GeneratePinSpritesCommand extends Command:
	const MapSettings = preload("res://Scripts/Settings/map_settings.gd")
	const NAME = "MapCommand.generate_pin_sprites_command"

	const PinSprite = preload("res://Scenes/OverworldMap/Pin.tscn")


	func execute(args = []):
		var scene = args[0]
		var map = args[1]
		var camera = args[2]
		var container = args[3]
		
		for pin_data in MapModel.pins:
			var pin_sprite = PinSprite.instance()
			pin_sprite.data = pin_data
			pin_sprite.position = pin_data.position
			pin_sprite.nearest_path_point = map.get_nearest_point_id(Vector3(pin_data.position.x, pin_data.position.y, 0))
			pin_sprite.camera = camera
			pin_sprite.connect("touched_pin", scene, "_handle_touch_pin")
			container.add_child(pin_sprite)
			

class CreatePinSpriteCommand extends Command:
	const MapSettings = preload("res://Scripts/Settings/map_settings.gd")
	const NAME = "MapCommand.create_pin_sprite_command"

	const PinSprite = preload("res://Scenes/OverworldMap/Pin.tscn")

	func execute(args = []):
		var scene = args[0]
		var map = args[1]
		var camera = args[2]
		var container = args[3]
		var pin_data = args[4]
		
		var pin_sprite = PinSprite.instance()
		pin_sprite.data = pin_data
		pin_sprite.position = pin_data.position
		pin_sprite.nearest_path_point = map.get_nearest_point_id(Vector3(pin_data.position.x, pin_data.position.y, 0))
		pin_sprite.camera = camera
		pin_sprite.connect("touched_pin", scene, "_handle_touch_pin")
		container.add_child(pin_sprite)


class CreateMapRoachCommand extends Command:
	const NAME = "MapCommand.create_map_roach_command"

	const RoachSprite = preload("res://Scenes/OverworldMap/Map_Roach.tscn")

	func execute(args = []):
		var scene = args[0]
		var roach_data = args[1]
		var map = args[2]
		var container = args[3]
		
		var roach_sprite = RoachSprite.instance()
		roach_sprite.data = roach_data
		roach_sprite.map = map
		roach_sprite.position = roach_data.position
		roach_sprite.connect("touched_roach", scene, "_handle_touch_roach")
		container.add_child(roach_sprite)
			
			