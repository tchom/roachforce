const EntitySprite =  preload("res://Scripts/Scenes/OverworldMap/entity_sprite.gd")

class OverworldMapUtilities extends Node:
	static func get_target_entity_parent(entity_name, map_node):
		match(entity_name):
			"LOCATION_ENTITY":
				return map_node.locations_node
			"HIVE_ENTITY":
				return map_node.entities_node
			"INFESTATION_ENTITY":
				return map_node.infestations_node

	static func create_entitysprite(texture_name, entity, position, parent):
		var texture = load("res://Assets/textures/overworld_map/overworld_map.sprites/"+ texture_name+".tres");
		var sprite = EntitySprite.new(entity)
		sprite.position = position
		sprite.texture = texture
		parent.add_child(sprite)


class Command:
	const NAME = "virtual_command"
	func _init(args = []):
		pass

	func execute(args = []):
		pass

class CreateOverworldMapCommand extends Command:
	const MapSettings = preload("res://Scripts/Settings/map_settings.gd")
	const NAME = "OverworldMapCommand.create_overworld_command"

	func execute(args = []):
		var map_node = args[0]

		for y in range(MapSettings.BLOCKS_DOWN*MapSettings.BLOCKS_TILES_DOWN):
			for x in range(MapSettings.BLOCKS_ACROSS*MapSettings.BLOCKS_TILES_ACROSS):
				var is_road = (x%4 == 0 || y%3 == 2)
				OverworldMap.create_tile(x, y, is_road)

		for block_y in range(MapSettings.BLOCKS_DOWN):
			for block_x in range(MapSettings.BLOCKS_ACROSS):
				var unlocked = ((block_x >= 2 && block_x <=5) && (block_y >= 2 && block_y <= 5))
				OverworldMap.assign_block(block_x, block_y, unlocked)

		create_locations(map_node)
		OverworldMap.create_game_deck()
		OverworldMap.is_created = true

	func create_locations(map_node):
		randomize()
		var locations_data = load_location_json()["locations"]
		var starter_blocks = []
		var late_game_blocks = []

		for block_y in range(MapSettings.BLOCKS_DOWN):
			for block_x in range(MapSettings.BLOCKS_ACROSS):
				var location_x = block_x*MapSettings.BLOCKS_TILES_ACROSS + 2
				var location_y = block_y*MapSettings.BLOCKS_TILES_DOWN + 1

				if((block_x >= 2 && block_x <=5) && (block_y >= 2 && block_y <= 5)):
					starter_blocks.append(Vector2(location_x, location_y))
				else:
					late_game_blocks.append(Vector2(location_x, location_y))

		# find starter locations
		var starter_locations = []
		for i in range(locations_data.size()):
			if(locations_data[i].start_location):
				starter_locations.append(locations_data[i])

		while(starter_locations.size() < MapSettings.NUMBER_OF_START_LOCATIONS):
			var location_index = randi() % locations_data.size()
			if(!starter_locations.has(locations_data[location_index])):
				starter_locations.append(locations_data[location_index])


		for starter_location in starter_locations:
			var starter_block_index = randi() % starter_blocks.size()
			var block = starter_blocks[starter_block_index]
			starter_blocks.remove(starter_block_index)
			
			OverworldMap.create_location_entity(block.x, block.y, starter_location)
			locations_data.erase(starter_location)



		# create rest of locations
		for j in range(MapSettings.LOCATIONS_PER_GAME - MapSettings.NUMBER_OF_START_LOCATIONS):
			var block_index = randi() % late_game_blocks.size()
			var block = late_game_blocks[block_index]
			late_game_blocks.remove(block_index)

			var location_index = randi() % locations_data.size()
			var location = locations_data[location_index]
			locations_data.remove(location_index)
			OverworldMap.create_location_entity(block.x, block.y, location)


	func load_location_json():
		var file = File.new()
		file.open("res://Data/locations.json", file.READ)
		var text_json = file.get_as_text()
		file.close()

		var result_json = JSON.parse(text_json)
		var result = {}

		if result_json.error == OK:  # If parse OK

			return result_json.result.data
		else:  # If parse has errors
			return null

class DrawOverworldTilesCommand extends Command:
	const MapSettings = preload("res://Scripts/Settings/map_settings.gd")
	const NAME = "OverworldMapCommand.draw_overworld_tiles_command"

	func execute(args = []):
		print("Draw tiles")
		var map_node = args[0]
		var tile_map = map_node.tile_map

		for block in OverworldMap.map_blocks:
			if(block.unlocked):
				var roadTile = tile_map.tile_set.find_tile_by_name("base/road_tile")
				var buildingTile = tile_map.tile_set.find_tile_by_name("base/building_tile")
				for tile in block.tiles:
					if(tile.is_road):
						tile_map.set_cell(tile.x, tile.y, roadTile)
					else:
						tile_map.set_cell(tile.x, tile.y, buildingTile)
			else:
				for tile in block.tiles:
					var lockedTile = tile_map.tile_set.find_tile_by_name("base/locked_tile")
					tile_map.set_cell(tile.x, tile.y, lockedTile)
		tile_map.create_astar_grid()

class CreateOverworldEntitySpritesCommand extends Command:
	const MapSettings = preload("res://Scripts/Settings/map_settings.gd")
	const NAME = "OverworldMapCommand.create_overworld_entity_sprites_command"


	func execute(args = []):
		var map_node = args[0]

		for tile in OverworldMap.map_tiles:
			for entity in tile.entities:
				var parent = OverworldMapUtilities.get_target_entity_parent(entity.NAME, map_node)
				var half_size = MapSettings.TILE_SIZE / 2
				var world_position = Vector2(entity.tile.x * MapSettings.TILE_SIZE + half_size, entity.tile.y * MapSettings.TILE_SIZE + half_size)
				OverworldMapUtilities.create_entitysprite(entity.texture_name, entity, world_position, parent)

				if(entity.NAME == "HIVE_ENTITY"):
					map_node.ai_director.add_hive_entity(entity)

class CreateHiveCommand extends Command:
	const MapSettings = preload("res://Scripts/Settings/map_settings.gd")
	const NAME = "OverworldMapCommand.create_hive_command"
	const EntitySprite =  preload("res://Scripts/Scenes/OverworldMap/entity_sprite.gd")

	func execute(args = []):
		var map_node = args[0]
		var entity = OverworldMap.reveal_hive()
		var parent = OverworldMapUtilities.get_target_entity_parent(entity.NAME, map_node)
		var half_size = MapSettings.TILE_SIZE / 2
		var world_position = Vector2(entity.tile.x * MapSettings.TILE_SIZE + half_size, entity.tile.y * MapSettings.TILE_SIZE + half_size)
		OverworldMapUtilities.create_entitysprite(entity.texture_name, entity, world_position, parent)
		map_node.ai_director.add_hive_entity(entity, true)

class InfestNextSpaceCommand extends Command:
	const MapSettings = preload("res://Scripts/Settings/map_settings.gd")
	const NAME = "OverworldMapCommand.infest_next_hive_command"

	func execute(args = []):
		var map_node = args[0]
		var hive = args[1]
		var entity = OverworldMap.infest_next_space(hive)

		if(entity):
			var parent = OverworldMapUtilities.get_target_entity_parent(entity.NAME, map_node)
			var half_size = MapSettings.TILE_SIZE / 2
			var world_position = Vector2(entity.tile.x * MapSettings.TILE_SIZE + half_size, entity.tile.y * MapSettings.TILE_SIZE + half_size)
			OverworldMapUtilities.create_entitysprite(entity.texture_name, entity, world_position, parent)

class UnblockBlockCommand extends Command:
	const MapSettings = preload("res://Scripts/Settings/map_settings.gd")
	const NAME = "OverworldMapCommand.unlock_block_command"

	func execute(args = []):
		OverworldMap.unlock_block()
		GameController.execute_command(DrawOverworldTilesCommand.NAME, [args[0]])



