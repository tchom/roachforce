

class Command:
	const NAME = "virtual_command"
	func _init(args = []):
		pass

	func execute(args = []):
		pass

class CreateUnitCommand extends Command:
	const NAME = "BattleCommand.create_unit_command"
	const SCATTER_RANGE = 10
	var units = {}

	func _init(args = []):
		# player units
		units["swatter"] = load("res://Scenes/Battles/Units/Player/Swatter.tscn")
		units["shooter"] = load("res://Scenes/Battles/Units/Player/Shooter.tscn")
		# enemy units
		units["baby_roach"] = load("res://Scenes/Battles/Units/Enemy/Baby_Roach.tscn")
		units["soldier_roach"] = load("res://Scenes/Battles/Units/Enemy/Soldier_Roach.tscn")
		units["mozzie_roach"] = load("res://Scenes/Battles/Units/Enemy/Mozzie_Roach.tscn")

	func execute(args = []):
		var unit_type = args[0]
		var position = args[1]
		var team = args[2]
		var container = args[3]
		var number = args[4] if args[4] else 1


		if(units.has(unit_type)):
			for i in range(number):
				var adjusted_position = Vector2(position.x + (randf()*SCATTER_RANGE-(SCATTER_RANGE/2)), position.y + (randf()*SCATTER_RANGE-(SCATTER_RANGE/2)))
				var unit_instance = units[unit_type].instance()
				unit_instance.position = adjusted_position
				unit_instance.team = team
				container.add_child(unit_instance)


class CreateProjectileCommand extends Command:
	const NAME = "BattleCommand.create_projectile_command"
	var projectiles = {}

	func _init(args = []):
		projectiles["arrow"] = load("res://Scenes/Battles/Projectiles/Arrow.tscn")

	func execute(args = []):
		var projectile_type = args[0]
		var position = args[1]
		var target = args[2]
		var damage = args[3]
		var container = args[4]

		if(projectiles.has(projectile_type)):
			var projectile_instance = projectiles[projectile_type].instance()
			projectile_instance.position = position
			projectile_instance.target = target
			projectile_instance.damage = damage
			container.add_child(projectile_instance)


class CreateEffectCommand extends Command:
	const NAME = "BattleCommand.create_effect_command"
	var effects = {}

	func _init(args = []):
		effects["blood"] = load("res://Scenes/Battles/Effects/Blood.tscn")
		effects["monster_blood"] = load("res://Scenes/Battles/Effects/Blood2.tscn")
		effects["melee"] = load("res://Scenes/Battles/Effects/Melee_Effect.tscn")

	func execute(args = []):
		var effect_type = args[0]
		var position = args[1]
		var container = args[2]
		
		if(effects.has(effect_type)):
			var effect_instance = effects[effect_type].instance()
			effect_instance.position = position
			container.add_child(effect_instance)



class CreateTimePowerCommand extends Command:
	const NAME = "BattleCommand.create_time_power_command"
	var projectiles = {}

	func _init(args = []):
		#projectiles["smoko"] = load("res://Scenes/Battles/Projectiles/Arrow.tscn")
		pass

	func execute(args = []):
		var time_power_type = args[0]
		var amount = args[1]

		Player.battle_time += amount
