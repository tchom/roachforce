extends Node2D

const OMC = preload("res://Scripts/Controllers/Commands/OverworldMap/overworld_map_commands.gd")
const OMMC = preload("res://Scripts/Controllers/Commands/OverworldMap/overworld_map_mobile_commands.gd")
const BC = preload("res://Scripts/Controllers/Commands/Battle/battle_commands.gd")

var commands = {}

func _ready():
	ready_commands()

func ready_commands():
	# overworld map commands
	commands[OMC.CreateOverworldMapCommand.NAME] = OMC.CreateOverworldMapCommand.new()
	commands[OMC.DrawOverworldTilesCommand.NAME] = OMC.DrawOverworldTilesCommand.new()
	commands[OMC.CreateOverworldEntitySpritesCommand.NAME] = OMC.CreateOverworldEntitySpritesCommand.new()
	commands[OMC.CreateHiveCommand.NAME] = OMC.CreateHiveCommand.new()
	commands[OMC.InfestNextSpaceCommand.NAME] = OMC.InfestNextSpaceCommand.new()
	commands[OMC.UnblockBlockCommand.NAME] = OMC.UnblockBlockCommand.new()

	# overworld map mobile commands
	commands[OMMC.InitialiseMapCommand.NAME] = OMMC.InitialiseMapCommand.new()
	commands[OMMC.GeneratePinSpritesCommand.NAME] = OMMC.GeneratePinSpritesCommand.new()
	commands[OMMC.CreatePinSpriteCommand.NAME] = OMMC.CreatePinSpriteCommand.new()
	commands[OMMC.CreateMapRoachCommand.NAME] = OMMC.CreateMapRoachCommand.new()

	# battle commands
	commands[BC.CreateProjectileCommand.NAME] = BC.CreateProjectileCommand.new()
	commands[BC.CreateUnitCommand.NAME] = BC.CreateUnitCommand.new()
	commands[BC.CreateEffectCommand.NAME] = BC.CreateEffectCommand.new()
	commands[BC.CreateTimePowerCommand.NAME] = BC.CreateTimePowerCommand.new()


func execute_command(name, args = []):
	if(commands.has(name)):
		return commands[name].execute(args)
	else:
		return null