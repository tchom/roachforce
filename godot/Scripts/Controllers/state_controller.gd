extends Node

var current_scene = null

var next_scene_path = "res://Insert/Scene/Here.tscn"
var next_scene_manifest = []

func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child( root.get_child_count() -1 )

func goto_state(path):

	# This function will usually be called from a signal callback,
	# or some other function from the running scene.
	# Deleting the current scene at this point might be
	# a bad idea, because it may be inside of a callback or function of it.
	# The worst case will be a crash or unexpected behavior.

	# The way around this is deferring the load to a later time, when
	# it is ensured that no code from the current scene is running:

	call_deferred("_deferred_goto_state",path)


func _deferred_goto_state(path):

	# Immediately free the current scene,
	# there is no risk here.
	current_scene.free()

	# Load new scene
	var scene = Resource_Manager.get_resource(path)

	if(scene == null):
		print("Not loaded: ")
		print(path)
		scene = ResourceLoader.load(path)

	# Instance the new scene
	current_scene = scene.instance()

	# Add it to the active scene, as child of root
	get_tree().get_root().add_child(current_scene)

	# optional, to make it compatible with the SceneTree.change_scene() API
	get_tree().set_current_scene( current_scene )

func goto_next_state():
	goto_state(next_scene_path)