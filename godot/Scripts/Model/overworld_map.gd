extends Node

const MapSettings = preload("res://Scripts/Settings/map_settings.gd")
var van_tile_position = Vector2(0,0)
var is_created = false
var map_tiles = []
var map_blocks = []
var map_locked_blocks = []
var map_unlocked_blocks = []
var entities = []
var hq_location_entity

var hive_in_combat

#AI ACTIONS DECK
enum ACTIONS { UNLOCK_BLOCK, FREE_TURN, REVEAL_HIVE, ADD_EVENT, REVEAL_BOSS_HIVE }
var actions_deck = []
var action_index = 0

func create_game_deck():
	# start with a free turn and a hive
	actions_deck.append(FREE_TURN)
	actions_deck.append(REVEAL_HIVE)
	# create chances
	var lesser_actions_chances = []
	for i in range(MapSettings.CHANCE_OF_FREE_TURN):
		lesser_actions_chances.append(FREE_TURN)
	for j in range(MapSettings.CHANCE_OF_UNLOCK_BLOCK):
		lesser_actions_chances.append(UNLOCK_BLOCK)
	for k in range(MapSettings.CHANCE_OF_ADD_EVENT):
		lesser_actions_chances.append(ADD_EVENT)

	var total_locked_blocks = 48
	while(total_locked_blocks > 0):
		var actions_until_next_hive = rand_range(MapSettings.MIN_ACTIONS_BETWEEN_HIVES, MapSettings.MAX_ACTIONS_BETWEEN_HIVES)
		for i in range(actions_until_next_hive):
			# decide if ACTION is FREE_TURN, UNLOCK_BLOCK, ADD_EVENT
			var lesser_action = lesser_actions_chances[randi() % lesser_actions_chances.size()]
			actions_deck.append(lesser_action)

			if(lesser_action == UNLOCK_BLOCK):
				total_locked_blocks -= 1
		actions_deck.append(REVEAL_HIVE)
	actions_deck.append(REVEAL_BOSS_HIVE)

func get_current_action():
	return actions_deck[action_index]

func next_action():
	action_index += 1
	
#METHODS
#ENTITY METHODS

func resolve_combat():
	if(hive_in_combat != null):
		destroy_hive(hive_in_combat)
		hive_in_combat = null

		# maybe create hive_detroyed enitity here

func destroy_hive(hive_entity):
	destroy_infestations(hive_entity)
	hive_entity.tile.entities.erase(hive_entity)
	hive_entity.queue_free()

func destroy_infestations(parent_hive):
	for infestation in parent_hive.infestations:
		infestation.tile.entities.erase(infestation)
		infestation.queue_free()
	parent_hive.infestations = []

func check_road_kill(position):
	var current_tile = get_tile_from_position(position.x, position.y)
	for entity in current_tile.entities:
		if entity.NAME == "INFESTATION_ENTITY":
			trim_infestation(entity.hive, entity)

func trim_infestation(hive, start_infestation):
	var infestation_index = hive.infestations.find(start_infestation)

	for i in range(infestation_index, hive.infestations.size()):
		var infestation =  hive.infestations[i]
		#infestation.sprite.queue_free()
		infestation.tile.entities.erase(infestation)
		infestation.queue_free()

	hive.infestations.resize(infestation_index)

func reveal_hive():
	var rand_block = map_unlocked_blocks[randi() % map_unlocked_blocks.size()]
	var rand_tile = rand_block.tiles[randi() % rand_block.tiles.size()]
	return create_hive_entity(rand_tile.x, rand_tile.y)

func infest_next_space(hive):
	if hive.stunned:
		return
	var next_space_index = hive.infestations.size()
	if(next_space_index < hive.infestations_path.size()):
		var path_point = hive.infestations_path[next_space_index]
		return infest_tile(hive, path_point.x, path_point.y)
	return null

func infest_tile(hive, x, y):
	var tile = get_tile_from_position(x, y)
	var infestation_entity = InfestationsEntity.new()
	infestation_entity.hive = hive
	entities.append(infestation_entity)
	infestation_entity.set_tile(tile)
	tile.entities.append(infestation_entity)
	hive.infestations.append(infestation_entity)
	infestation_entity.texture_name = "entities/infested"
	return infestation_entity

func unlock_block():
	var unlocked_valid_block = false
	while(!unlocked_valid_block && map_locked_blocks.size() > 0):
		var random_block_index = randi() % map_locked_blocks.size()
		var rand_locked_block = map_locked_blocks[random_block_index]
		# check if this block is next to unlocked block
		if has_unlocked_neighbour(rand_locked_block.x, rand_locked_block.y):
			rand_locked_block.unlocked = true
			map_locked_blocks.remove(random_block_index)
			unlocked_valid_block = true
			map_unlocked_blocks.append(rand_locked_block)


func has_unlocked_neighbour(x, y):
	if block_is_within_bounds(x-1, y):
		if (get_block_from_position(x-1, y).unlocked):
			return true

	if block_is_within_bounds(x+1, y):
		if (get_block_from_position(x+1, y).unlocked):
			return true

	if block_is_within_bounds(x, y-1):
		if (get_block_from_position(x, y-1).unlocked):
			return true

	if block_is_within_bounds(x, y+1):
		if (get_block_from_position(x, y+1).unlocked):
			return true
	return false

func block_is_within_bounds(x, y):
	return (x >= 0 && x < MapSettings.BLOCKS_ACROSS && y >= 0 && y < MapSettings.BLOCKS_DOWN)

func tile_is_within_bounds(x, y):
	return (x >= 0 && x < MapSettings.BLOCKS_ACROSS*MapSettings.BLOCKS_TILES_ACROSS && y >= 0 && y < MapSettings.BLOCKS_DOWN*MapSettings.BLOCKS_TILES_DOWN)



# CREATION METHODS
func create_location_entity(x, y, location_data):
	var tile = get_tile_from_position(x, y)
	var locaton_entity = LocationEntity.new()
	locaton_entity.set_tile(tile)
	entities.append(locaton_entity)
	tile.entities.append(locaton_entity)

	locaton_entity.texture_name = location_data.sprite

	if location_data.has("is_hq") && location_data.is_hq:
		hq_location_entity = locaton_entity

	return locaton_entity

func create_hive_entity(x, y):
	var tile = get_tile_from_position(x, y)
	var hive_entity = HiveEntity.new(tile, hq_location_entity)
	entities.append(hive_entity)
	tile.entities.append(hive_entity)
	hive_entity.texture_name = "entities/hive"
	return hive_entity

func create_tile(x, y, is_road):
	var map_tile = MapTile.new(x, y, is_road)
	map_tiles.append(map_tile)

func assign_block(x, y, unlocked):
	var block = MapBlock.new(x, y, unlocked)

	for j in range(MapSettings.BLOCKS_TILES_DOWN):
		for i in range(MapSettings.BLOCKS_TILES_ACROSS):
			var block_start_x = x*MapSettings.BLOCKS_TILES_ACROSS
			var block_start_y = y*MapSettings.BLOCKS_TILES_DOWN
			var tile_x = block_start_x + i
			var tile_y = block_start_y + j
			var tile = get_tile_from_position(tile_x, tile_y)
			tile.unlocked = block.unlocked
			block.add_tile(tile)
	map_blocks.append(block)

	if(block.unlocked):
		map_unlocked_blocks.append(block)
	else:
		map_locked_blocks.append(block)

func get_world_position_from_tile_position(x, y):
	return Vector2(x*MapSettings.TILE_SIZE + MapSettings.TILE_SIZE/2, y*MapSettings.TILE_SIZE + MapSettings.TILE_SIZE/2)

func get_block_from_position(x, y):
	return map_blocks[x + MapSettings.BLOCKS_ACROSS * y]

func get_tile_from_position(x, y):
	return map_tiles[x + (MapSettings.BLOCKS_ACROSS*MapSettings.BLOCKS_TILES_ACROSS) * y]

func get_tile_from_world_position(x, y):
	var tileX = round(x/MapSettings.TILE_SIZE)
	var tileY = round(y/MapSettings.TILE_SIZE)
	return map_tiles[tileX + (MapSettings.BLOCKS_ACROSS*MapSettings.BLOCKS_TILES_ACROSS) * tileY]

func get_tile_neightbourhood_from_position(x, y):
	var tiles = []

	if tile_is_within_bounds(x, y):
		var tile = get_tile_from_position(x, y)
		tiles.append(tile)

	if tile_is_within_bounds(x, y-1):
		var tile = get_tile_from_position(x, y-1)
		tiles.append(tile)
		
	if tile_is_within_bounds(x, y+1):
		var tile = get_tile_from_position(x, y+1)
		tiles.append(tile)
		
	if tile_is_within_bounds(x-1, y):
		var tile = get_tile_from_position(x-1, y)
		tiles.append(tile)
		
	if tile_is_within_bounds(x+1, y):
		var tile = get_tile_from_position(x+1, y)
		tiles.append(tile)
		
	return tiles


# CLASSES
class MapEntity extends Node:
	signal updated
	const NAME = "VIRTUAL_MAP_ENTITY"
	var tile
	var unlocked setget unlocked_set
	var texture_name

	func _init():
		pass

	func set_tile(new_tile):
		tile = new_tile
		
	func unlocked_set(new_value):
		unlocked = new_value
		emit_signal("updated")

class LocationEntity extends MapEntity:
	const NAME = "LOCATION_ENTITY"



class HiveEntity extends MapEntity:
	const NAME = "HIVE_ENTITY"
	var stunned = true
	var infestations = []
	var infestations_path = []
	var target_location


	func _init(new_tile, new_target):
		target_location = new_target
		set_tile(new_tile)
		_calculate_hive_path(Vector2(tile.x, tile.y), Vector2(target_location.tile.x, target_location.tile.y))

	func _calculate_hive_path(start, end):
		var dx = (end.x - start.x)
		var dy = (end.y - start.y)

		var nx = abs(dx)
		var ny = abs(dy)

		var sign_x = 1 if (dx > 0) else -1
		var sign_y = 1 if (dy > 0) else -1

		var current = Vector2(start.x, start.y)
		#infestations_path.append(Vector2(current.x, current.y))
		var ix = 0
		var iy = 0

		while(ix < nx || iy < ny):
			if (ny == 0):
				current.x += sign_x
				ix+=1
			elif (nx == 0):
				current.y += sign_y
				iy+=1
			elif ((0.5+ix) / nx < (0.5+iy) / ny):
				current.x += sign_x
				ix+=1
			else:
				current.y += sign_y
				iy+=1

			infestations_path.append(Vector2(current.x, current.y))


class InfestationsEntity extends MapEntity:
	const NAME = "INFESTATION_ENTITY"
	var hive

	func set_tile(new_tile):
		tile = new_tile


class MapTile:
	var x
	var y
	var is_road = false
	var entities = []
	var map_block
	var unlocked = false setget unlocked_set

	func _init(_x, _y, _is_road):
		x = _x
		y = _y
		is_road = _is_road

	func unlocked_set(new_value):
		unlocked = new_value
		for entity in entities:
			entity.unlocked = new_value


class MapBlock:
	var x
	var y
	var unlocked = false setget unlocked_set
	var tiles = []

	func _init(_x, _y, _unlocked):
		x = _x
		y = _y
		unlocked = _unlocked

	func add_tile(tile):
		tiles.append(tile)

	func unlocked_set(new_value):
		unlocked = new_value
		for tile in tiles:
			tile.unlocked = new_value
