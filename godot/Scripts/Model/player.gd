extends Node

signal time_expired

var van_speed = 5

var van_power = 0 setget van_power_set
var van_max_power = 1500

var max_energy = 100
var starting_energy = 30
var energy = 0
var active_card_limit = 4

var battle_start_time = 90.0
var battle_time = 0.0 setget battle_time_set

var card_deck =[
		{type="unit", name="swatter", amount=3, energy_price=30},
		{type="unit", name="swatter", amount=3, energy_price=30},
		{type="unit", name="swatter", amount=3, energy_price=30},
		{type="unit", name="shooter", amount=2, energy_price=40},
		{type="unit", name="swatter", amount=3, energy_price=30},
		{type="unit", name="swatter", amount=3, energy_price=30},
		{type="unit", name="swatter", amount=3, energy_price=30},
		{type="time_power", name="smoko", amount=30, energy_price=40},
		{type="time_power", name="smoko", amount=30, energy_price=40}
	]

func delta_energy(delta):
	energy += delta*5
	energy = min(energy, max_energy)

func has_enough_energy(value):
	return value <= energy

func pay_energy(value):
	if value > energy:
		return false
	else:
		energy -= value
		return true

func battle_time_set(value):
	if value < 0:
		battle_time = 0.0
		emit_signal("time_expired")
	else:
		battle_time = value

func van_power_set(value):
	if value < 0:
		van_power = 0
	elif value > van_max_power:
		van_power = van_max_power
	else:
		van_power = value