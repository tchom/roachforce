extends Node

enum PIN_TYPE{
  LOCATION,
  HOME,
  EVENT,
  HIVE
}

enum ROACH_TYPE{
  ROAMER,
  SPEED,
  HUNTER,
  DESTROYER
}

const ROAMER_CHANCE = 30
const SPEED_CHANCE = 20
const HUNTER_CHANCE = 10
const DESTROYER_CHANCE = 10

const MapSettings = preload("res://Scripts/Settings/map_settings.gd")

var initilialised = false
var pins = []
var home_pin
var location_pins = []
var hive_pins = []
var activated_pins = []
var roaches = []

func initilialise(pin_locations):
	for pin_loc in pin_locations.get_children():
		var new_pin = PinData.new(pin_loc.get_index(), pin_loc.position, pin_loc.type)

		if(pin_loc.type == HOME):
			home_pin = new_pin
			activate_pin(new_pin)
		elif(pin_loc.type == HIVE):
			add_hive_pin(new_pin)
		elif(pin_loc.type == LOCATION):
			add_location_pin(new_pin)

		pins.push_back(new_pin)

	#activate random start locations
	while(activated_pins.size() < MapSettings.NUMBER_OF_START_LOCATIONS):
		var rand_index = floor(rand_range(0,location_pins.size()))
		var possible_pin = location_pins[rand_index]

		if(!activated_pins.has(possible_pin)):
			activate_pin(possible_pin)

	initilialised = true

func add_location_pin(pin):
	location_pins.push_back(pin)

func add_hive_pin(pin):
	hive_pins.push_back(pin)

func activate_pin(pin):
	pin.activated = true
	activated_pins.push_back(pin)

func create_rand_roach(pos):
	var cumulative_prob_range = ROAMER_CHANCE + SPEED_CHANCE + HUNTER_CHANCE + DESTROYER_CHANCE
	var random_result = rand_range(1, cumulative_prob_range)
	var roach_data
	if(random_result < ROAMER_CHANCE):
		roach_data = RoachData.new(pos, ROAMER)
	elif(random_result < ROAMER_CHANCE + SPEED_CHANCE):
		roach_data = RoachData.new(pos, SPEED)
	elif(random_result < ROAMER_CHANCE + SPEED_CHANCE + HUNTER_CHANCE):
		roach_data = RoachData.new(pos, HUNTER)
	elif(random_result < ROAMER_CHANCE + SPEED_CHANCE + HUNTER_CHANCE + DESTROYER_CHANCE):
		roach_data = RoachData.new(pos, DESTROYER)

	return roach_data

func activate_random_hive():
	if(hive_pins.size() > 0):
		var rand_index = floor(rand_range(0,hive_pins.size()))
		var new_pin = hive_pins[rand_index]
		activate_pin(new_pin)

		hive_pins.remove(rand_index)

		return new_pin

	else:
		return null


class PinData:
	var index
	var position
	var type
	var activated = false

	func _init(new_index, new_position, new_type):
		index = new_index
		type = new_type
		position = new_position


class RoachData:
	var position
	var type = 0
	var energy = 1000
	func _init(new_position, new_type):
		type = new_type
		position = new_position