extends Node2D

func _ready():
	var load_manifest = []
	load_manifest.append("res://Assets/textures/ui/splash/company_logo.png")
	load_manifest.append("res://Assets/textures/ui/main_menu/background.jpg")
	load_manifest.append("res://Assets/textures/ui/main_menu/game_logo.png")
	load_manifest.append("res://Assets/textures/ui/main_menu/menu_ui.png")
	load_manifest.append("res://Scenes/Splash.tscn")
	load_manifest.append("res://Scenes/MainMenu.tscn")

	State_Controller.next_scene_manifest = load_manifest
	State_Controller.next_scene_path = "res://Scenes/Splash.tscn"
	State_Controller.goto_state("res://Scenes/Load.tscn")