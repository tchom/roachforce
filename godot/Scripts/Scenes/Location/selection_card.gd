extends TextureButton

signal was_clicked

const Button_Atlas_Up = preload("res://Assets/textures/ui/battle/battle_ui.sprites/card_up.tres")
const Button_Atlas_Over = preload("res://Assets/textures/ui/battle/battle_ui.sprites/card_over.tres")
const Button_Atlas_Down = preload("res://Assets/textures/ui/battle/battle_ui.sprites/card_down.tres")
const Button_Atlas_Disabled = preload("res://Assets/textures/ui/battle/battle_ui.sprites/card_disabled.tres")

const WIDTH = 128
const HEIGHT = 200


var _unit_icon = null
var card_data = null

var _energy_cost_label = null



func _init(card_data):
	self.card_data = card_data

func _ready():
	rect_size = Vector2(128, 200)

	mouse_default_cursor_shape = CURSOR_POINTING_HAND

	texture_normal = Button_Atlas_Up
	texture_pressed = Button_Atlas_Down
	texture_hover = Button_Atlas_Over
	texture_disabled = Button_Atlas_Disabled

	
	_energy_cost_label = RichTextLabel.new()
	_energy_cost_label.bbcode_enabled = true
	_energy_cost_label.mouse_filter = MOUSE_FILTER_IGNORE
	_energy_cost_label.rect_size = Vector2(WIDTH, HEIGHT)
	_energy_cost_label.bbcode_text = str("[right]"+str(card_data.energy_price)+"[/right]")
	_energy_cost_label.margin_right = WIDTH - 5
	_energy_cost_label.margin_top = 5
	add_child(_energy_cost_label)

	_unit_icon = TextureRect.new()
	_unit_icon.mouse_filter = MOUSE_FILTER_IGNORE
	_unit_icon.texture = load("res://Assets/textures/ui/battle/battle_ui.sprites/icon/"+card_data.name+".tres")
	_unit_icon.rect_position = Vector2((WIDTH - _unit_icon.texture.region.size.x)/2, (HEIGHT - _unit_icon.texture.region.size.y)/2)
	add_child(_unit_icon)

