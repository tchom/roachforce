extends Node2D
const Selection_card = preload('selection_card.gd')

onready var _return_button = get_node("Return_Button")
onready var _button_options =$Button_Options

func _ready():
	var card_options =[
		{type="unit", name="swatter", amount=3, energy_price=30},
		{type="unit", name="shooter", amount=2, energy_price=40},
		{type="time_power", name="smoko", amount=30, energy_price=40}
	]

	for i in range(card_options.size()):
		var card_data = card_options[i]
		var player_card = Selection_card.new(card_data)
		_button_options.add_child(player_card)

		player_card.connect("pressed", self, "_handle_select_button", [player_card])


func _handle_select_button(button):
	Player.card_deck.append(button.card_data)
	
	var load_manifest = []	
	State_Controller.next_scene_manifest = load_manifest
	State_Controller.next_scene_path = "res://Scenes/OverworldMap/OverworldMap.tscn"
	State_Controller.goto_state("res://Scenes/Load.tscn")

func _handle_click():
	var load_manifest = []	
	print("LEAVE")
	State_Controller.next_scene_manifest = load_manifest
	State_Controller.next_scene_path = "res://Scenes/OverworldMap/OverworldMap.tscn"
	State_Controller.goto_state("res://Scenes/Load.tscn")