extends Node2D

const OMMC = preload("res://Scripts/Controllers/Commands/OverworldMap/overworld_map_mobile_commands.gd")
const MapSettings = preload("res://Scripts/Settings/map_settings.gd")
const VAN_REFUEL_SPEED = 1500

onready var camera = $Camera2D
onready var van = $Van
onready var map = $Map
onready var pin_canvas = $Pin_Canvas
onready var pin_locations = $Pin_Locations
onready var ai_director = $AI_Director

onready var alert = get_node("PlayerUI/Alert")

var home_pin

enum STATES { PLAYER_TURN, AI_TURN, RESTORE_VAN_ENERGY }
var _state = null

func _ready():
	set_process_input(true)

	van.connect("van_out_of_energy", self, "_handle_van_out_of_energy")
	van.connect("destination_reached", self, "_handle_destination_reached")
	ai_director.connect("ai_turn_complete", self, "_handle_ai_turn_complete")
	ai_director.connect("spawn_hive", self, "_handle_spawn_hive")
	ai_director.connect("spawn_roach", self, "_handle_spawn_roach")
	ai_director.connect("display_alert", self, "_handle_display_alert")
	ai_director.camera = camera
	
	if(!MapModel.initilialised):
		GameController.execute_command(OMMC.InitialiseMapCommand.NAME, [pin_locations])
	
	var home_pos_3d = Vector3(MapModel.home_pin.position.x, MapModel.home_pin.position.y, 0)
	var home_path_point = map.get_nearest_point(home_pos_3d)
	van.position = MapModel.home_pin.position
	camera.position = MapModel.home_pin.position
	van.closest_point = map.get_nearest_point(home_path_point)

	for pin_loc in pin_locations.get_children():
		pin_loc.hide()


	GameController.execute_command(OMMC.GeneratePinSpritesCommand.NAME, [self, map, camera, pin_canvas])
	_change_state(STATES.PLAYER_TURN)
	Player.van_power = Player.van_max_power

func _process(delta):
	if(_state == STATES.PLAYER_TURN):
		pass
	elif(_state == STATES.AI_TURN):
		pass
	elif(_state == STATES.RESTORE_VAN_ENERGY):
		if(Player.van_power < Player.van_max_power):
			Player.van_power += VAN_REFUEL_SPEED*delta
		else:
			_change_state(STATES.PLAYER_TURN)
		pass
	

func _change_state(new_state):
	if(_state == new_state):
		return

	if(new_state == STATES.PLAYER_TURN):
		_state = new_state
		pass
	elif(new_state == STATES.AI_TURN):
		ai_director.start_turn()
		_state = new_state
		pass
	elif(new_state == STATES.RESTORE_VAN_ENERGY):
		_state = new_state
		pass

func _handle_ai_turn_complete():
	alert.hide()
	_change_state(STATES.RESTORE_VAN_ENERGY)


func _handle_van_out_of_energy():
	_change_state(STATES.AI_TURN)

func _handle_spawn_hive(pin_data):
	GameController.execute_command(OMMC.CreatePinSpriteCommand.NAME, [self, map, camera, pin_canvas, pin_data])

func _handle_spawn_roach(nearest_path_point):
	var position = map.get_point_position(nearest_path_point)
	var roach_data = MapModel.create_rand_roach(Vector2(position.x, position.y))
	GameController.execute_command(OMMC.CreateMapRoachCommand.NAME, [self, roach_data, map, ai_director])

func _handle_touch_roach(roach_position):
	if(_state == STATES.PLAYER_TURN):
		var nearest_van_id = map.get_nearest_point_id(Vector3(van.position.x, van.position.y, 0))
		var nearest_roach_id = map.get_nearest_point_id(Vector3(roach_position.x, roach_position.y, 0))
		var path = map.get_path(nearest_van_id, nearest_roach_id)
		path.push_back(Vector3(roach_position.x, roach_position.y, 0))
		van.handle_move(path)
		camera.set_track_target(van)

func _handle_display_alert(alert_text):
	alert.text = alert_text;
	alert.show()

func _handle_touch_pin(nearest_path_point):
	if(_state == STATES.PLAYER_TURN):
		var nearest_van_id = map.get_nearest_point_id(Vector3(van.position.x, van.position.y, 0))
		var path = map.get_path(nearest_van_id, nearest_path_point)
		van.handle_move(path)
		camera.set_track_target(van)
	
func _handle_destination_reached(reached_target):
	if(reached_target):
		var load_manifest = []	
		State_Controller.next_scene_manifest = load_manifest
		State_Controller.next_scene_path = "res://Scenes/Battles/Maps/DummyBattle1.tscn"
		State_Controller.goto_state("res://Scenes/Load.tscn")
	else:
		_change_state(STATES.AI_TURN)
	
