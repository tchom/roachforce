extends Node2D

const CHANCE_OF_HIVE = 40
const CHANCE_OF_SPAWN = 5
const CHANCE_OF_NOTHING = 50

signal ai_turn_complete
signal display_alert
signal spawn_hive
signal spawn_roach

enum STATES { ACTIVE, INACTIVE }
var _state = STATES.INACTIVE

var camera

#placeholder only
const AI_MAX_TIME = 3
const AI_SHORT_TIME = 1
var timeout_time = AI_SHORT_TIME;
var ai_time = 0


func start_turn():
	_change_state(STATES.ACTIVE)
	

func _process(delta):
	if(_state == STATES.ACTIVE):
		ai_time += delta

		if(ai_time > timeout_time):
			emit_signal("ai_turn_complete")
			_change_state(STATES.INACTIVE)

func determine_action():
	move_roaches()

	var cumulative_chances = CHANCE_OF_HIVE + CHANCE_OF_SPAWN + CHANCE_OF_NOTHING
	var random_result = rand_range(0, cumulative_chances)
	timeout_time = AI_SHORT_TIME

	if random_result < CHANCE_OF_HIVE:
		# Do hive
		emit_signal("display_alert", "ALERT! HIVE DETECTED!")
		var pin_data = MapModel.activate_random_hive()
		camera.reveal_position(pin_data.position)
		emit_signal("spawn_hive", pin_data)
		timeout_time = AI_MAX_TIME

	elif random_result < CHANCE_OF_HIVE + CHANCE_OF_SPAWN:
		# Do hive
		var hives = get_tree().get_nodes_in_group("active_hives")
		if(hives.size() > 0):
			emit_signal("display_alert", "ALERT! BUG ATTACK!")

		for hive_pin in hives:
			emit_signal("spawn_roach", hive_pin.nearest_path_point)

		timeout_time = AI_SHORT_TIME

	

func move_roaches():
	var roaches = get_tree().get_nodes_in_group("active_roaches")
	for roach in roaches:
		roach.activate()

func _change_state(new_state):
	if(new_state == STATES.ACTIVE):
		ai_time = 0
		determine_action()
		_state = new_state
		pass
	elif(new_state == STATES.INACTIVE):
		_state = new_state
		pass
