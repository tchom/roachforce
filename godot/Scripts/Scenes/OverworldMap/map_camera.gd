extends Camera2D

enum STATES { FREE_MOVE, TRACKING, REVEAL_POSITION }
var _state = null
var _track_target
var _reveal_position
var _is_tracking
const TRACK_ARRIVE_DIST = 2;
const REVEAL_ARRIVE_DIST = 100;

var first_distance =0
var events={}
var percision = 10
var current_zoom
var maximum_zoomin = Vector2(0.5,0.5)
var tracking_zoom = Vector2(1.5,1.5)
var minimum_zoomout = Vector2(4,4)
var center

var state = {}
var _os2own = {}
var is_zooming = false
var prev_pinch_diff = 0


func _ready():
	set_process_unhandled_input(true)
	_state = FREE_MOVE
	pass

func set_track_target(target):
	_track_target = target
	_track_target.connect("end_camera_track", self, "_handle_end_camera_track")
	_state = TRACKING
	_is_tracking = true

func reveal_position(reveal_pos):
	_reveal_position = reveal_pos
	_state = REVEAL_POSITION
	_is_tracking = true

func _handle_end_camera_track():
	_track_target.disconnect("end_camera_track", self, "_handle_end_camera_track")
	_state = FREE_MOVE

func _process(delta):
	if OS.is_debug_build():
		# vertical
		if Input.is_key_pressed(KEY_W): # do action here
			position -= Vector2(0, 2000)*delta
		elif Input.is_key_pressed(KEY_S): # do action here
			position += Vector2(0, 2000)*delta
		# horizontal
		if Input.is_key_pressed(KEY_A): # do action here
			position -= Vector2(2000, 0)*delta
		elif Input.is_key_pressed(KEY_D): # do action here
			position += Vector2(2000, 0)*delta

	if(_state == REVEAL_POSITION):
		position = (position*49 + _reveal_position) / 50
		zoom = (zoom*49 + minimum_zoomout)/50
		if(position.distance_squared_to(_reveal_position) < REVEAL_ARRIVE_DIST*REVEAL_ARRIVE_DIST):
			_is_tracking = false
			print("end reveal")
			_state = FREE_MOVE
	elif(_state == TRACKING || _is_tracking):
		position = (position*9 + _track_target.position) / 10
		zoom = (zoom*49 + tracking_zoom)/50
		
		if(position.distance_squared_to(_track_target.position) < TRACK_ARRIVE_DIST*TRACK_ARRIVE_DIST):
			_is_tracking = false


func _unhandled_input(event):
	if(_state == FREE_MOVE):
		if event is InputEventScreenTouch:
			if event.pressed:
				# Down
				if !_os2own.has(event.index): # Defensively discard index if already known
					var ptr_id = _find_free_pointer_id()
					state[ptr_id] = event.position
					_os2own[event.index] = ptr_id
			else:
				# Up
				is_zooming = false
				if _os2own.has(event.index): # Defensively discard index if not known
					var ptr_id = _os2own[event.index]
					state.erase(ptr_id)
					_os2own.erase(event.index)

		elif event is InputEventScreenDrag:
			# Move
			if _os2own.has(event.index): # Defensively discard index if not known
				var ptr_id = _os2own[event.index]
				state[ptr_id] = event.position

		# pan
		if event is InputEventScreenDrag and state.size() == 1:
			position -= event.relative * zoom.x
		#pinch
		elif event is InputEventScreenDrag and state.size() == 2 and not is_zooming:
			is_zooming = true
			prev_pinch_diff = state[0].distance_squared_to(state[1])

			return true
		elif event is InputEventScreenDrag and state.size() == 2 and is_zooming:
			is_zooming = true

			var pinch_diff = state[0].distance_squared_to(state[1])
			var delta_pinch = prev_pinch_diff/pinch_diff

			zoom *= delta_pinch

			prev_pinch_diff = pinch_diff

		if event is InputEventMouseButton:
			if event.is_pressed():
				# zoom in
				if event.button_index == BUTTON_WHEEL_UP:
					_zoom_camera(-1)
				if event.button_index == BUTTON_WHEEL_DOWN:
					_zoom_camera(1)

		truncate_zoom()


func _zoom_camera(dir):
    zoom += Vector2(0.3, 0.3) * dir

func truncate_zoom():
	if(zoom.length_squared ( ) < maximum_zoomin.length_squared ( )):
		zoom = maximum_zoomin
	

	if(zoom.length_squared ( ) > minimum_zoomout.length_squared ( )):
		zoom = minimum_zoomout



func _find_free_pointer_id():
	var used = state.keys()
	var i = 0
	while i in used:
		i += 1
	return i