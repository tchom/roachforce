extends Node2D

# You can only create an AStar node from code, not from the Scene tab
onready var astar_node = AStar.new()

var _map_data = null
var _all_points = []

var _points_dict = {}
var _point_index_dict = {}

func _init():
	var file = File.new()
	file.open("res://Data/Map/map_data.json", file.READ)
	var jsonText = file.get_as_text()
	_map_data = parse_json(jsonText)["data"]
	file.close()

	for segment in _map_data["segments"]:
		var start = Vector3(segment["start"]["x"], segment["start"]["y"], 0)
		var end = Vector3(segment["end"]["x"], segment["end"]["y"], 0)
		
		if(!_all_points.has(start)):
			_all_points.push_back(start)

		if(!_all_points.has(end)):
			_all_points.push_back(end)

		

func _ready():
	for i in range(0, _all_points.size()):
		_point_index_dict[_all_points[i]] = i
		astar_node.add_point(i, _all_points[i], 1)

	for segment in _map_data["segments"]:
		var start = Vector3(segment["start"]["x"], segment["start"]["y"], 0)
		var end = Vector3(segment["end"]["x"], segment["end"]["y"], 0)

		var start_index = _point_index_dict[start]
		var end_index = _point_index_dict[end]

		astar_node.connect_points(start_index, end_index)
		

func get_nearest_point(point):
	if(point == null):
		return null
		
	var nearest_point = null
	
	var nearest_dist = 1e20
	for other_point in _all_points:
		var other_dist = point.distance_squared_to(other_point)
		if(other_dist < nearest_dist):
			nearest_dist = other_dist
			nearest_point = other_point

	return nearest_point

func get_point_id(point):
	return _point_index_dict[point]

func get_point_position (id):
	return astar_node.get_point_position(id)


func get_nearest_point_id(point):
	var nearest_point = get_nearest_point(point)
	return get_point_id(nearest_point)

func get_path(startPointId, endPointId):
	return astar_node.get_point_path(startPointId, endPointId)
