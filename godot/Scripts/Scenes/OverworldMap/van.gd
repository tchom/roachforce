extends Position2D

signal end_camera_track
signal destination_reached
signal van_out_of_energy

export(float) var SPEED = 400.0

enum STATES { MOVING, WAITING }
var _state = null

var current_path
var target_point_world = Vector2()
var velocity = Vector2()

var closest_point


func _ready():
	_change_state(WAITING)

func _change_state(new_state):
	if new_state == MOVING:
		if not current_path or len(current_path) == 1:
			_change_state(WAITING)
			return
		# The index 0 is the starting cell
		# we don't want the character to move back to it in this example
		target_point_world = Vector2(current_path[1].x, current_path[1].y)
		
	elif new_state == WAITING:
		
		pass

	_state = new_state


func _process(delta):
	if not _state == MOVING:
		return

	if(Player.van_power <= 0):
		emit_signal("end_camera_track")
		emit_signal("destination_reached", false)
		_change_state(WAITING)
		return

	var arrived_to_next_point = move_to(target_point_world)
	if arrived_to_next_point:
		current_path.remove(0)
		if len(current_path) == 0:
			emit_signal("end_camera_track")
			emit_signal("destination_reached", true)
			_change_state(WAITING)
			return

		target_point_world = Vector2(current_path[0].x, current_path[0].y)
		#emit_signal("check_road_kill")


func move_to(world_position):
	var MASS = 3.0
	var ARRIVE_DISTANCE = 15.0

	var desired_velocity = (world_position - position).normalized() * SPEED
	var steering = desired_velocity - velocity
	velocity += steering / MASS

	position += velocity * get_process_delta_time()
	
	Player.van_power -= velocity.length() * get_process_delta_time()

	return position.distance_to(world_position) < ARRIVE_DISTANCE



func handle_move(path):
	if _state == WAITING:
		current_path = path
		_change_state(MOVING)


