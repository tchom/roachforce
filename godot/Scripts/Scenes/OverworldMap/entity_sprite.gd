extends Sprite
var entity
var entity_wr

func _init(new_entity):
	entity = new_entity
	entity.connect("updated", self, "_handle_update")
	entity_wr = weakref(entity)
	_handle_update()

func _handle_update():
	if(!entity.tile.unlocked):
		hide()
	else:
		show()

func _process(delta):
	if(!entity_wr.get_ref()):
		queue_free()