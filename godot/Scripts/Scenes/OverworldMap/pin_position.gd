tool

extends Position2D

const MapModel = preload("res://Scripts/Model/overworld_map_mobile.gd")
export (MapModel.PIN_TYPE) var type = 0 setget type_set


func type_set(value):
	if(value == MapModel.PIN_TYPE.LOCATION):
		get_node("Marker").animation = "location"
	elif(value == MapModel.PIN_TYPE.HIVE):
		get_node("Marker").animation = "hive"
	elif(value == MapModel.PIN_TYPE.EVENT):
		get_node("Marker").animation = "event"
	elif(value == MapModel.PIN_TYPE.HOME):
		get_node("Marker").animation = "home"
	type = value