extends Area2D

signal touched_roach

var data
var map

enum STATES { ACTIVE, INACTIVE }
var _state = STATES.INACTIVE

onready var sprite = $AnimatedSprite

var current_path
var current_energy
var target_point_world

export(float) var SPEED = 400.0
var velocity = Vector2()

func _ready():
	add_to_group("active_roaches")

func _process(delta):
	if(_state == STATES.ACTIVE):

		if(current_energy <= 0):
			#emit_signal("end_camera_track")
			_change_state(STATES.INACTIVE)
			return

		var arrived_to_next_point = move_to(target_point_world, delta)
		if arrived_to_next_point && current_path != null:
			current_path.remove(0)
			if len(current_path) == 0:
				"""emit_signal("end_camera_track")
				emit_signal("destination_reached")"""
				_change_state(STATES.INACTIVE)
				return

			target_point_world = Vector2(current_path[0].x, current_path[0].y)



func move_to(world_position, delta):
	if(world_position == null):
		_state = STATES.INACTIVE
		return
		
	var MASS = 3.0
	var ARRIVE_DISTANCE = 15.0

	var desired_velocity = (world_position - position).normalized() * SPEED
	var steering = desired_velocity - velocity
	velocity += steering / MASS

	position += velocity * delta
	
	current_energy -= velocity.length() * delta

	return position.distance_to(world_position) < ARRIVE_DISTANCE

func roamer_activate():
	var rand_location = MapModel.location_pins[randi() % MapModel.location_pins.size()]
	var nearest_roach_id = map.get_nearest_point_id(Vector3(position.x, position.y, 0))
	var nearest_location_id = map.get_nearest_point_id(Vector3(rand_location.position.x, rand_location.position.y, 0))
	current_path = map.get_path(nearest_roach_id, nearest_location_id)
	target_point_world = Vector2(current_path[0].x, current_path[0].y)

func speed_activate():
	pass

func hunter_activate():
	pass

func detroyer_activate():
	pass

func activate():
	_change_state(STATES.ACTIVE)

func _change_state(new_state):
	if(new_state == STATES.ACTIVE):
		_state = new_state
		current_energy = data.energy
		roamer_activate()

	elif(new_state == STATES.INACTIVE):
		_state = new_state
		pass


func _input_event(viewport, event, shape_idx):
	if (event is InputEventMouseButton && event.pressed && (event.button_index != BUTTON_WHEEL_UP &&  event.button_index != BUTTON_WHEEL_DOWN)):
		emit_signal("touched_roach", position)
