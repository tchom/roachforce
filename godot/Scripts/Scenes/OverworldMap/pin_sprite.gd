extends Area2D

signal touched_pin

var data
var camera
onready var sprite = $PinSprite

var activated setget activated_set

export(int)var nearest_path_point = 0

func _ready():
	self.activated = data.activated
	position = data.position

	if(data.type == MapModel.PIN_TYPE.HOME):
		sprite.animation = "home"
	if(data.type == MapModel.PIN_TYPE.LOCATION):
		sprite.animation = "location"
	if(data.type == MapModel.PIN_TYPE.HIVE):
		sprite.animation = "hive"


func _process(delta):
	if(data):
		#position = position2D.get_global_transform_with_canvas ( ).get_origin ( )
		scale = camera.zoom
		pass

func _input_event(viewport, event, shape_idx):
	if (event is InputEventMouseButton && event.pressed && (event.button_index != BUTTON_WHEEL_UP &&  event.button_index != BUTTON_WHEEL_DOWN)):
		emit_signal("touched_pin", nearest_path_point)

func activated_set(value):
	activated = value
	if(activated == true):
		show()
		if(data.type == MapModel.PIN_TYPE.HIVE && !is_in_group("active_hives")):
			add_to_group("active_hives")
	else:
		hide()
		if(data.type == MapModel.PIN_TYPE.HIVE && is_in_group("active_hives")):
			remove_from_group("active_hives")
