extends Label

const BLINK_SWITCH_TIME = 0.5
var blink_time = 0
var is_on = true

func _process(delta):
	blink_time += delta

	if(blink_time > BLINK_SWITCH_TIME):
		is_on = !is_on
		blink_time = 0

		if(is_on):
			add_color_override("font_color", Color(1,0,0,1))
		else:
			add_color_override("font_color", Color(1,0,0,0))

	
	