extends ProgressBar

onready var label = $Label

func _process(delta):
	value = (Player.van_power / Player.van_max_power)*100

	label.text = str(floor(Player.van_power)) + "/" + str(Player.van_max_power)