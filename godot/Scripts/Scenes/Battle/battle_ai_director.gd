extends Node


enum STATES { BATTLE_RUNNING, BATTLE_END }
var _state = null

var _current_time = 0
var _next_attack_index = 0

var _attack_data = null
onready var _unit_container = get_parent().get_node("Unit_Container")

var team_id

func _ready():
	#load attack 
	_loadAttackData()
	_change_state(BATTLE_RUNNING)


func _loadAttackData():
	var file = File.new()
	file.open("res://Data/Battles/basic_battle.json", file.READ)
	var jsonText = file.get_as_text()
	_attack_data = parse_json(jsonText)["data"]
	file.close()


func _process(delta):
	if(_state == BATTLE_RUNNING):
		handle_battle_running(delta)

func _change_state(new_state):
	if new_state == BATTLE_RUNNING:
		pass
	elif new_state == BATTLE_END:
		pass
	else:
		pass
	_state = new_state

func handle_battle_end(delta):
	pass

func handle_battle_running(delta):
	_current_time = _current_time + (delta*0.1)
	var nextAttack = _attack_data["attacks"][_next_attack_index]
	if _current_time > nextAttack.time :
		var spawnCoordinates = Vector2(nextAttack.coordinates[0], nextAttack.coordinates[1])
		GameController.execute_command("BattleCommand.create_unit_command", [nextAttack.unit_type, spawnCoordinates, GameSettings.ENEMY, _unit_container, nextAttack.amount])
		_next_attack_index += 1

		#reset cycle
		if _next_attack_index >= _attack_data["attacks"].size():
			_current_time = 0
			_next_attack_index = 0

func end_battle():
	_change_state(BATTLE_END)