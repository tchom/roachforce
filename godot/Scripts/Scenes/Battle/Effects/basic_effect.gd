extends Node2D

func _ready():
	var animSprite = get_node("AnimatedSprite")
	animSprite.play()
	animSprite.connect("animation_finished", self, "handle_anim_complete")
	
	

func handle_anim_complete():
	queue_free()