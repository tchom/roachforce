extends Control
const Player_card = preload('player_card.gd')

onready var card_drop_area = $Card_Drop_Area
onready var card_holder = $Card_Holder
onready var out_of_bounds_hint = get_parent().get_node("Out_Of_Bounds_Hint")

var _buttons = []

enum STATES { BATTLE_RUNNING, BATTLE_END }
var _state = null

func _ready():
	card_drop_area.connect("handle_drop_card", self, "_handle_card_drop")

	_buttons = []

	for i in range(Player.card_deck.size()):
		var card_data = Player.card_deck[i]
		var player_card = Player_card.new(card_data)
		card_holder.add_child(player_card)
		player_card.connect("start_dragging", self, "_handle_start_dragging")
		player_card.connect("stop_dragging", self, "_handle_stop_dragging")
		_buttons.append(player_card)

	_change_state(BATTLE_RUNNING)

func _handle_start_dragging():
	out_of_bounds_hint.show_hint(true)

func _handle_stop_dragging():
	out_of_bounds_hint.show_hint(false)

func _handle_card_drop(button):
	card_holder.move_child(button, card_holder.get_child_count() -1)


func _process(delta):
	if(_state == BATTLE_RUNNING):
		handle_battle_running(delta)

	


func _change_state(new_state):
	if new_state == BATTLE_RUNNING:
		pass
	elif new_state == BATTLE_END:
		pass
	else:
		pass
	_state = new_state

func handle_battle_end(delta):
	pass

func handle_battle_running(delta):
	for button in _buttons:
		if(button.get_index() >= Player.active_card_limit):
			button.visible = false
		else:
			button.visible = true
	
func end_battle():
	_change_state(BATTLE_END)