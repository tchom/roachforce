extends ProgressBar

onready var label = $Label

func _process(delta):
	value = (Player.energy / Player.max_energy)*100

	label.text = str(floor(Player.energy)) + "/" + str(Player.max_energy)