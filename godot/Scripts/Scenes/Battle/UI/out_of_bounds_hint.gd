extends ColorRect

func show_hint(do_show):
	if(do_show):
		show()
	else:
		hide()