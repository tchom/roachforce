extends ReferenceRect
signal handle_drop_card

onready var unit_container = get_tree().get_root().get_node("BattleScene/Unit_Container")

func can_drop_data(pos, data):
	return true


func drop_data(pos, data):
	# create unit
	Player.pay_energy(data.energy_price)

	if(data.type == "unit"):
		GameController.execute_command("BattleCommand.create_unit_command", [data.name, rect_position + pos, GameSettings.PLAYER, unit_container, data.amount])
	elif (data.type == "time_power"):
		GameController.execute_command("BattleCommand.create_time_power_command", [data.name, data.amount])

	emit_signal("handle_drop_card", data.button)