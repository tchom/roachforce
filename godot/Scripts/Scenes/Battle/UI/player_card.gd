extends TextureButton

signal start_dragging
signal stop_dragging

const Button_Atlas_Up = preload("res://Assets/textures/ui/battle/battle_ui.sprites/card_up.tres")
const Button_Atlas_Over = preload("res://Assets/textures/ui/battle/battle_ui.sprites/card_over.tres")
const Button_Atlas_Down = preload("res://Assets/textures/ui/battle/battle_ui.sprites/card_down.tres")
const Button_Atlas_Disabled = preload("res://Assets/textures/ui/battle/battle_ui.sprites/card_disabled.tres")

const WIDTH = 128
const HEIGHT = 200


var _unit_icon = null
var _drop_data = null
var _preview_atlas = null

var _energy_cost_label = null

var _is_dragging = false


func _init(drop_data):
	_drop_data = drop_data
	_drop_data.button = self

func _ready():
	rect_size = Vector2(128, 200)

	mouse_default_cursor_shape = CURSOR_CAN_DROP

	texture_normal = Button_Atlas_Up
	texture_pressed = Button_Atlas_Down
	texture_hover = Button_Atlas_Over
	texture_disabled = Button_Atlas_Disabled
	_preview_atlas = load("res://Assets/textures/ui/battle/battle_ui.sprites/drag_preview/"+_drop_data.name+".tres")

	
	_energy_cost_label = RichTextLabel.new()
	_energy_cost_label.bbcode_enabled = true
	_energy_cost_label.mouse_filter = MOUSE_FILTER_IGNORE
	_energy_cost_label.rect_size = Vector2(WIDTH, HEIGHT)
	_energy_cost_label.bbcode_text = str("[right]"+str(_drop_data.energy_price)+"[/right]")
	_energy_cost_label.margin_right = WIDTH - 5
	_energy_cost_label.margin_top = 5
	add_child(_energy_cost_label)

	_unit_icon = TextureRect.new()
	_unit_icon.mouse_filter = MOUSE_FILTER_IGNORE
	_unit_icon.texture = load("res://Assets/textures/ui/battle/battle_ui.sprites/icon/"+_drop_data.name+".tres")
	_unit_icon.rect_position = Vector2((WIDTH - _unit_icon.texture.region.size.x)/2, (HEIGHT - _unit_icon.texture.region.size.y)/2)
	add_child(_unit_icon)


func _process(delta):
	disabled = !(Player.has_enough_energy(_drop_data.energy_price))

	if(disabled):
		mouse_default_cursor_shape = CURSOR_FORBIDDEN
		_unit_icon.modulate = Color(1, 1, 1, 0.5)

	else:
		mouse_default_cursor_shape = CURSOR_CAN_DROP
		_unit_icon.modulate = Color(1, 1, 1, 1)

	
	if(_is_dragging &&  Input.is_action_just_released("click")):
		#stopped dragging
		_is_dragging = false
		emit_signal("stop_dragging")


func get_drag_data(pos):
	if(disabled):
		return null
	var control = Control.new()
	var tr = TextureRect.new()
	tr.texture = _preview_atlas
	tr.rect_position = Vector2(-_unit_icon.texture.region.size.x/2, -_unit_icon.texture.region.size.y/2)
	control.add_child(tr)
	set_drag_preview(control)
	emit_signal("start_dragging")
	return _drop_data


func can_drop_data(pos, data):
	_is_dragging = true
	return data == _drop_data


func drop_data(pos, data):
	_drop_data=data
