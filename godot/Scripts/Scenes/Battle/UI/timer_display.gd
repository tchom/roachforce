extends Label

enum TimeFormat {
    FORMAT_HOURS   = 1 << 0,
    FORMAT_MINUTES = 1 << 1,
    FORMAT_SECONDS = 1 << 2,
    FORMAT_TENSOFSECONDS = 1 << 3,
    FORMAT_DEFAULT = FORMAT_HOURS | FORMAT_MINUTES | FORMAT_SECONDS | FORMAT_TENSOFSECONDS
}

const URGENT_TIME = 30 
const DEFAULT_COLOR = Color(1,1,1,1) 
const URGENT_COLOR = Color(1,0,0,1)
var is_in_urgent_countdown = false

var is_running = false

func _ready():
	Player.battle_time = Player.battle_start_time
	is_running = true

func _process(delta):
	if(is_running):
		text = format_time(Player.battle_time)

		if(Player.battle_time < URGENT_TIME && !is_in_urgent_countdown):
			is_in_urgent_countdown = true
			add_color_override("font_color", URGENT_COLOR)
		elif (Player.battle_time > URGENT_TIME && is_in_urgent_countdown):
			is_in_urgent_countdown = false
			add_color_override("font_color", DEFAULT_COLOR)




func format_time(time, format = FORMAT_DEFAULT, digit_format = "%02d"):
	var digits = []

	if format & FORMAT_HOURS:
		var hours = digit_format % [time / 3600]
		#digits.append(hours)

	if format & FORMAT_MINUTES:
		var minutes = digit_format % [time / 60]
		digits.append(minutes)

	if format & FORMAT_SECONDS:
		var seconds = digit_format % [int(floor(time)) % 60]
		digits.append(seconds)

	if format & FORMAT_TENSOFSECONDS:
		var tens_of_seconds = str(time - floor(time)).substr(2,2)
		if tens_of_seconds.empty():
			tens_of_seconds = "00"

		digits.append(tens_of_seconds)
		
	var formatted = String()
	var colon = " : "

	for idx in digits.size():
		formatted += digits[idx]
		if idx != digits.size() - 1:
			formatted += colon

	return formatted