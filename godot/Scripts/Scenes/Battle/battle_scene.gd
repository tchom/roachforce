extends Node2D


enum STATES { BATTLE_RUNNING, BATTLE_END }
var _state = null

const GAME_OVER_COOLDOWN = 5

var van_wr
var boss_wr
var game_over_ticker = 0
var player_victory = false

onready var AI_Director = $AI_Director
onready var Player_UI = $Player_UI

func _ready():
	OS.screen_orientation = OS.SCREEN_ORIENTATION_PORTRAIT
	Player.energy = Player.starting_energy
	van_wr = weakref(get_node("Unit_Container/Van"))
	boss_wr =  weakref(get_node("Unit_Container/Boss"))

	Player.connect("time_expired", self, "_handle_time_expired")
	_change_state(BATTLE_RUNNING)


func _process(delta):
	if(_state == BATTLE_RUNNING):
		handle_battle_running(delta)
	elif(_state == BATTLE_END):
		handle_battle_end(delta)

func _change_state(new_state):
	if new_state == BATTLE_RUNNING:
		pass
	elif new_state == BATTLE_END:
		AI_Director.end_battle()
		Player_UI.end_battle()
	else:
		pass
	_state = new_state


func handle_battle_running(delta):
	Player.battle_time -= delta
	Player.delta_energy(delta)

	if(!van_wr.get_ref()):
		player_victory = false
		_change_state(BATTLE_END)
		print("GAME OVER - YOU LOSE")


	if(!boss_wr.get_ref()):
		player_victory = true
		_change_state(BATTLE_END)
		print("VICTORY!")


func handle_battle_end(delta):
	game_over_ticker += delta
		
	if(game_over_ticker > GAME_OVER_COOLDOWN):
		State_Controller.next_scene_manifest = []
		State_Controller.next_scene_path = "res://Scenes/OverworldMap/OverworldMapMobile.tscn"
		State_Controller.goto_state("res://Scenes/Load.tscn")

func _handle_time_expired():
	print("TIMES UP")
	print("GAME OVER - YOU LOSE")
	_change_state(BATTLE_END)