extends "res://Scripts/Scenes/Battle/Units/StateMachine/state.gd"
var nav2d
var target

func enter():
	if(nav2d == null):
		nav2d = get_tree().get_root().get_node("BattleScene/Navigation2D")
	if(target == null):
		target = get_tree().get_root().get_node("BattleScene/"+owner.target_name)

	owner.current_path = nav2d.get_navigation_path(owner.position, target.position)
	emit_signal("finished", "follow_path")