extends "res://Scripts/Scenes/Battle/Units/StateMachine/state.gd"

func enter():
	owner.current_path = null

func update(delta):
	owner.check_for_targets()

	if(!owner.has_target):
		owner.is_engaged = false
		emit_signal("finished", "search_for_new_target")
		return

	if owner.target_unit.has_method("get_vehicle"):
		owner.vehicle.pursue(owner.target_unit, delta)
	else:
		owner.vehicle.seek(owner.target_unit.position)

	if(owner.weapon.target_in_range(owner.target_unit)):
		emit_signal("finished", "attack")