extends "res://Scripts/Scenes/Battle/Units/StateMachine/state.gd"
const ARRIVE_DIST_SQUARED = 20*20

func enter():
	if(owner.current_path == null || owner.current_path.size() == 0):
		emit_signal("finished", "find_path_to_boss")

func update(delta):
	#check if unit has an enemy to pursue
	if(owner.has_target):
		emit_signal("finished", "move_to_attack")
		return

	var path = owner.current_path
	var next_point = path[0]
	
	var distSq = owner.position.distance_squared_to(next_point)
	
	if(distSq < ARRIVE_DIST_SQUARED):
		path.remove(0)
		
		if(path.size() == 0):
			emit_signal("finished", "idle")
			return
			
		next_point = path[0]
	owner.vehicle.seek(next_point)
	owner.current_path = path


