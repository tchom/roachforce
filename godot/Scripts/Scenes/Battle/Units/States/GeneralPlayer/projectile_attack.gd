extends "res://Scripts/Scenes/Battle/Units/StateMachine/state.gd"

func enter():
	owner.is_engaged = true

func update(delta):
	owner.vehicle.velocity = Vector2(0,0)

	if(!owner.has_target):
		owner.is_engaged = false
		emit_signal("finished", "search_for_new_target")
		return

	# make sure target is still in range
	if(!owner.weapon.target_in_range(owner.target_unit)):
		owner.is_engaged = false
		emit_signal("finished", "move_to_attack")
		return


	if(owner.weapon.is_off_cooldown):
		#owner.target_unit.take_damage(owner.weapon.attack())
		var damage = owner.weapon.attack()
		GameController.execute_command("BattleCommand.create_projectile_command", ["arrow", owner.position, owner.target_unit, damage, owner.get_parent()])
