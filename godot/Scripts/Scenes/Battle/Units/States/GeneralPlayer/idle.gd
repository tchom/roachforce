extends "res://Scripts/Scenes/Battle/Units/StateMachine/state.gd"

func enter():
	owner.vehicle.velocity = Vector2(0, 0)

func update(delta):
	#check if unit has an enemy to pursue
	if(owner.has_target):
		emit_signal("finished", "move_to_attack")
