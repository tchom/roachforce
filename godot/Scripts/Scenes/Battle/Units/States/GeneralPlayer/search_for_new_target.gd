extends "res://Scripts/Scenes/Battle/Units/StateMachine/state.gd"

func enter():
	owner.check_for_targets()
	if(owner.has_target):
		emit_signal("finished", "move_to_attack")
	else:
		emit_signal("finished", "find_path_to_boss")