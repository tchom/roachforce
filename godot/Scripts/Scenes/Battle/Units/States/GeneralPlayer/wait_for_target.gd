extends "res://Scripts/Scenes/Battle/Units/StateMachine/state.gd"

func enter():
	owner.current_path = null

func update(delta):
	owner.check_for_targets()

	if(owner.has_target):

		if(owner.weapon.target_in_range(owner.target_unit)):
			emit_signal("finished", "boss_attack")