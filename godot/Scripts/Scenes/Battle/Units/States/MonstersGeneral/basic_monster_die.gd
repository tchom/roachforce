extends "res://Scripts/Scenes/Battle/Units/StateMachine/state.gd"

func enter():
	GameController.execute_command("BattleCommand.create_effect_command", ["monster_blood", owner.position, owner.get_parent()])
	owner.kill()
