extends PhysicsBody2D


var current_path

var target_name = "Player_Target"
export var health = 100
export (int) var team = GameSettings.TEAM.PLAYER

var has_target = false setget , has_target_get
var is_dead = false
var is_engaged = false


onready var state_machine = $StateMachine
onready var aggro = $Aggro_Range
onready var weapon = $Weapon

var target_unit = null setget ,target_unit_get


func _enter_tree():
	if(team == GameSettings.TEAM.PLAYER):
		target_name = "Player_Target"
	else:
		target_name = "Enemy_Target"

	set_physics_process(true)
	set_process(true)

func _ready():
	aggro.connect("body_entered", self, "handle_aggro")

func _process(delta):
	if(is_dead):
		return

	weapon.update(delta)



func handle_aggro(body):
	check_for_targets()

func check_for_targets():
	if(is_engaged):
		return

	var new_target = null
	var distSquaredBody = -1

	for body in aggro.get_overlapping_bodies () :
		if(weapon.can_target(body)):
			var newDist = body.position.distance_squared_to(position)
			if(distSquaredBody < 0 || newDist < distSquaredBody):
				distSquaredBody = newDist
				new_target = weakref(body)
	target_unit = new_target

func take_damage(damange_int):
	health = health - damange_int
	if(health <= 0):
		state_machine.change_state("die")
	
func kill():
	if(!is_dead):
		is_dead = true
		queue_free()

#Getters/Setters
func has_target_get():
	return target_unit != null && target_unit.get_ref() != null 

func target_unit_get():
	return target_unit.get_ref()
