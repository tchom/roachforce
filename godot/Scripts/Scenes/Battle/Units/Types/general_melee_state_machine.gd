extends "res://Scripts/Scenes/Battle/Units/StateMachine/state_machine.gd"

func initialize(start_state):
	states_map = {
		"follow_path": $FollowPath,
		"find_path_to_boss": $FindPathToBoss,
		"idle": $Idle,
		"move_to_attack": $MoveToAttack,
		"attack": $Attack,
		"search_for_new_target": $SearchForNewTarget,
		"die": $Die
	}
	.initialize(start_state)