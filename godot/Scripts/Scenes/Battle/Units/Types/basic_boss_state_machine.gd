extends "res://Scripts/Scenes/Battle/Units/StateMachine/state_machine.gd"

func initialize(start_state):
	states_map = {
		"wait_for_target": $WaitForTarget,
		"boss_attack": $BossAttack,
		"die": $Die
	}
	.initialize(start_state)