extends Node

# vehicle physics
export var mass = 0.1
export var max_speed = 200
var velocity = Vector2()
export var maxForce = 1
export var arriveThreshold = 100
var steeringForce = Vector2()
var position = Vector2()

# wander
var wanderAngle = 0
var wanderDistance = 10
var wanderRadius = 5
var wanderRange = 1

func process_velocity():
	position = get_parent().position
	steeringForce = steeringForce.clamped(maxForce)
	steeringForce = steeringForce / mass
	velocity = velocity + steeringForce
	steeringForce = Vector2()

	velocity = velocity.clamped(max_speed)

func seek(target):
	var desiredVelocity = target - position
	desiredVelocity = desiredVelocity.normalized()
	desiredVelocity = desiredVelocity * max_speed
	var force = desiredVelocity - velocity
	steeringForce = steeringForce + force


func flee(target):
	var desiredVelocity = target - position
	desiredVelocity = desiredVelocity.normalized()
	desiredVelocity = desiredVelocity * max_speed
	var force = desiredVelocity - velocity
	steeringForce = steeringForce - force


func arrive(target):
	var desiredVelocity = target - position
	desiredVelocity = desiredVelocity.normalized()

	var dist = position.distance_to(target)
	if(dist > arriveThreshold):
		desiredVelocity = desiredVelocity * max_speed
	else:
		desiredVelocity = desiredVelocity * (max_speed * dist / arriveThreshold)

	var force = desiredVelocity - velocity
	steeringForce = steeringForce + force

func pursue(targetUnit, delta):
	var lookAheadTime = position.distance_to(targetUnit.position)/max_speed
	var predictedTarget = targetUnit.position + (targetUnit.vehicle.velocity * lookAheadTime * delta)
	seek(predictedTarget)

func evade(targetUnit, delta):
	var lookAheadTime = position.distance_to(targetUnit.position)/max_speed
	var predictedTarget = targetUnit.position - (targetUnit.vehicle.velocity * lookAheadTime * delta)
	flee(predictedTarget)

func wander():
	var center = velocity.normalized() * wanderDistance
	var offset = Vector2(wanderRadius, 0)
	offset = offset.rotated(wanderAngle)
	wanderAngle = wanderAngle + (randf()*wanderRange-wanderRange*0.5)
	var force = center + offset
	steeringForce = steeringForce + force
