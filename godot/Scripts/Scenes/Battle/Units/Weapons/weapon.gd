extends Node
export var damage = 10
export var attack_range = 36
export (float, 0.1, 10.0) var max_cooldown = 1.5

var cooldown = 0.0
var is_on_cooldown setget ,is_on_cooldown_get
var is_off_cooldown setget ,is_on_cooldown_get


func _ready():
	owner = get_parent()

func update(deltaTime):
	if(cooldown > 0):
		cooldown = cooldown - deltaTime

func attack():
	return damage

func can_target(unit):
	return false

func target_in_range(unit):
	return false

func is_on_cooldown_get():
	return cooldown <= 0

func is_off_cooldown_get():
	return !is_on_cooldown