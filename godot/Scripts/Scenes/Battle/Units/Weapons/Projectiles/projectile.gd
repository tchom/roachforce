extends Node2D
export(float) var SPEED = 200.0
var target setget target_set
var damage
var velocity = Vector2()
var _prev_position


func _process(delta):
	if(target.get_ref() != null):
		_prev_position = target.get_ref().position
	elif(_prev_position == null && target.get_ref() == null):
		queue_free()
		return
		

	var arrived_at_target = move_to(_prev_position)
	if arrived_at_target:
		if(target.get_ref() != null):
			target.get_ref().take_damage(damage)
		queue_free()


func move_to(target_position):
	var MASS = 10.0
	var ARRIVE_DISTANCE = 10.0

	var desired_velocity = (target_position - position).normalized() * SPEED
	var steering = desired_velocity - velocity
	velocity += steering / MASS
	position += velocity * get_process_delta_time()
	rotation = velocity.angle()
	return position.distance_to(target_position) < ARRIVE_DISTANCE
	
func target_set(value):
	target = weakref(value)