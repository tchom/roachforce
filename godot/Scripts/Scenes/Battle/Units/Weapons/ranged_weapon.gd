extends "weapon.gd"

func attack():
	cooldown = max_cooldown
	return damage

func target_in_range(unit):
	var dist_sqr_to_target = owner.position.distance_squared_to(owner.target_unit.position)
	return (attack_range*attack_range) > dist_sqr_to_target

func can_target(unit):
	return (unit.team != owner.team)

