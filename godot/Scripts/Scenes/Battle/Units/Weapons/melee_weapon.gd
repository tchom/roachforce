extends "weapon.gd"

func attack():
	cooldown = max_cooldown
	return damage

func target_in_range(unit):
	var collisions = owner.get_colliding_bodies () 
	for collider in collisions:
		if(collider != null && collider == unit):
			return true
	return false

func can_target(unit):
	return (unit.team != owner.team && !unit.is_in_group("flying"))