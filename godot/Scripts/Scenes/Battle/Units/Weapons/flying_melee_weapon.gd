extends "weapon.gd"

func attack():
	cooldown = max_cooldown
	return damage

func target_in_range(unit):
	if(unit == null):
		return false
	var distSquared = owner.position.distance_squared_to(unit.position)
	return (distSquared < (attack_range * attack_range))

func can_target(unit):
	return (unit.team != owner.team)