extends "unit.gd"

export var speed = 200
onready var vehicle = $Vehicle


func _ready():
	vehicle.max_speed = speed

#movement behaviours
func _physics_process(delta):
	vehicle.process_velocity()

func _integrate_forces(state):
	state.linear_velocity = vehicle.velocity

func get_vehicle():
	return vehicle