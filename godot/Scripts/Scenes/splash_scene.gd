extends Node2D

var logo = null

func _ready():
	logo = get_node("logo")
	logo.get_node("AnimationPlayer").play("animate_in_logo")
	logo.get_node("AnimationPlayer").connect("animation_finished", self, "handle_anim_complete")

func handle_anim_complete(args):
	print("Goto_main menu")
	State_Controller.goto_state("res://Scenes/MainMenu.tscn")

func _input(event):
	if event is InputEventMouseButton:
		print("Goto_main menu")
		State_Controller.goto_state("res://Scenes/MainMenu.tscn")