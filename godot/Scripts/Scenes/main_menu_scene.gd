extends Node2D

var play_btn = null

func _ready():
	play_btn = get_node("play_btn")
	play_btn.connect("pressed", self, "handle_click")

func handle_click():
	var load_manifest = []	

	State_Controller.next_scene_manifest = load_manifest
	State_Controller.next_scene_path = "res://Scenes/OverworldMap/OverworldMapMobile.tscn"
	State_Controller.goto_state("res://Scenes/Load.tscn")