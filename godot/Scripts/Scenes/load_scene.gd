extends Node2D

var loaded_index = 0
var load_queue = []
var loader
var wait_frames
var time_max = 100
var loading_bar = null

func _ready():
	
	loading_bar = get_node("LoadingProgress")
	
	load_queue = State_Controller.next_scene_manifest

	wait_frames = 1

	if load_queue.size() > 0:
		loader = ResourceLoader.load_interactive(load_queue[loaded_index])


func _process(delta):
	if loader == null:
		# no need to process anymore
		set_process(false)
		goto_next_state()
		return

	if wait_frames > 0: # wait for frames to let the "loading" animation to show up
		wait_frames -= 1
		return

	var t = OS.get_ticks_msec()
	while OS.get_ticks_msec() < t + time_max: # use "time_max" to control how much time we block this thread

		# poll your loader
		var err = loader.poll()

		if err == ERR_FILE_EOF: # load finished
			Resource_Manager.add_resource(load_queue[loaded_index], loader.get_resource())
			loaded_index += 1
			if(loaded_index >= load_queue.size()):
				loader = null
				loading_bar.value = 100
				goto_next_state()
			else:
				load_next_queue_item()
			break
		elif err == OK:
			update_progress()
		else: # error during loading
			show_error()
			loader = null
			break

func load_next_queue_item():
	loader = ResourceLoader.load_interactive(load_queue[loaded_index])
	

func update_progress():
	var current_progress = float(loader.get_stage()) / loader.get_stage_count()
	 
	# update your progress bar?
	loading_bar.value = ((loaded_index + current_progress) / load_queue.size())*100

func goto_next_state():
	State_Controller.goto_next_state()