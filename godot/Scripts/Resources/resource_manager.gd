extends Node

var resources = {}

func add_resource(key, resource):
	resources[key] = resource

func get_resource(key):
	if(resources.has(key)):
		return resources[key]
	else:
		return null