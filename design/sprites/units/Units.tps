<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.8.3</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>godot3-spritesheet</string>
        <key>textureFileName</key>
        <filename>../../../godot/Assets/textures/units/units.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../../godot/Assets/textures/units/units.tpsheet</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>0</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">archer/team1/walk/people_25.png</key>
            <key type="filename">archer/team1/walk/people_26.png</key>
            <key type="filename">archer/team1/walk/people_27.png</key>
            <key type="filename">archer/team1/walk/people_28.png</key>
            <key type="filename">archer/team2/walk/people_05.png</key>
            <key type="filename">archer/team2/walk/people_06.png</key>
            <key type="filename">archer/team2/walk/people_07.png</key>
            <key type="filename">archer/team2/walk/people_08.png</key>
            <key type="filename">baby_roach/baby_roach.png</key>
            <key type="filename">bat/fly/monster_189.png</key>
            <key type="filename">bat/fly/monster_190.png</key>
            <key type="filename">bat/fly/monster_191.png</key>
            <key type="filename">bug/walk/monster_01.png</key>
            <key type="filename">bug/walk/monster_02.png</key>
            <key type="filename">footsoldier/team1/people_09.png</key>
            <key type="filename">footsoldier/team1/people_10.png</key>
            <key type="filename">footsoldier/team1/people_11.png</key>
            <key type="filename">footsoldier/team1/people_12.png</key>
            <key type="filename">footsoldier/team2/walk/people_29.png</key>
            <key type="filename">footsoldier/team2/walk/people_30.png</key>
            <key type="filename">footsoldier/team2/walk/people_31.png</key>
            <key type="filename">footsoldier/team2/walk/people_32.png</key>
            <key type="filename">ice_golem/walk/monster_135.png</key>
            <key type="filename">ice_golem/walk/monster_136.png</key>
            <key type="filename">juggernaught/team1/walk/people_85.png</key>
            <key type="filename">juggernaught/team1/walk/people_86.png</key>
            <key type="filename">juggernaught/team1/walk/people_87.png</key>
            <key type="filename">juggernaught/team1/walk/people_88.png</key>
            <key type="filename">juggernaught/team2/walk/people_105.png</key>
            <key type="filename">juggernaught/team2/walk/people_106.png</key>
            <key type="filename">juggernaught/team2/walk/people_107.png</key>
            <key type="filename">juggernaught/team2/walk/people_108.png</key>
            <key type="filename">knight/team1/walk/people_13.png</key>
            <key type="filename">knight/team1/walk/people_14.png</key>
            <key type="filename">knight/team1/walk/people_15.png</key>
            <key type="filename">knight/team1/walk/people_16.png</key>
            <key type="filename">knight/team2/walk/people_33.png</key>
            <key type="filename">knight/team2/walk/people_34.png</key>
            <key type="filename">knight/team2/walk/people_35.png</key>
            <key type="filename">knight/team2/walk/people_36.png</key>
            <key type="filename">mage/team1/walk/people_81.png</key>
            <key type="filename">mage/team1/walk/people_82.png</key>
            <key type="filename">mage/team1/walk/people_83.png</key>
            <key type="filename">mage/team1/walk/people_84.png</key>
            <key type="filename">mage/team2/walk/people_101.png</key>
            <key type="filename">mage/team2/walk/people_102.png</key>
            <key type="filename">mage/team2/walk/people_103.png</key>
            <key type="filename">mage/team2/walk/people_104.png</key>
            <key type="filename">minotaur/walk/monster_115.png</key>
            <key type="filename">minotaur/walk/monster_116.png</key>
            <key type="filename">necromancer/walk/monster_227.png</key>
            <key type="filename">necromancer/walk/monster_228.png</key>
            <key type="filename">peasant/team1/walk/people_21.png</key>
            <key type="filename">peasant/team1/walk/people_22.png</key>
            <key type="filename">peasant/team1/walk/people_23.png</key>
            <key type="filename">peasant/team1/walk/people_24.png</key>
            <key type="filename">peasant/team2/walk/people_41.png</key>
            <key type="filename">peasant/team2/walk/people_42.png</key>
            <key type="filename">peasant/team2/walk/people_43.png</key>
            <key type="filename">peasant/team2/walk/people_44.png</key>
            <key type="filename">rat/walk/monster_03.png</key>
            <key type="filename">rat/walk/monster_04.png</key>
            <key type="filename">skeleton/walk/monster_65.png</key>
            <key type="filename">skeleton/walk/monster_66.png</key>
            <key type="filename">soldier_roach/soldier_roach.png</key>
            <key type="filename">zombie/walk/monster_79.png</key>
            <key type="filename">zombie/walk/monster_80.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">health_bar/health_bar_backing.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,5,40,10</rect>
                <key>scale9Paddings</key>
                <rect>20,5,40,10</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">health_bar/health_bar_progress.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>19,3,37,5</rect>
                <key>scale9Paddings</key>
                <rect>19,3,37,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">towers/team1/tower_1.png</key>
            <key type="filename">towers/team2/tower2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,20,32,40</rect>
                <key>scale9Paddings</key>
                <rect>16,20,32,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">van/van.png</key>
            <key type="filename">viceroy/viceroy.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,32,64,64</rect>
                <key>scale9Paddings</key>
                <rect>32,32,64,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>.</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
